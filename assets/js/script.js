
// Clothing Nav Link on Hover show clothing hover state div and hide other link hover state 
$('#clothing').mouseenter(function(){
    $('.nav-link').css({'border-bottom-color': 'transparent'})
    $(this).css({'border-bottom-color': '#6458A2'})
    $('#clothing-hover-state').collapse("toggle")
    $('#jewelry-hover-state').collapse("hide")
    $('#art-hover-state').collapse("hide")
    $('#music-hover-state').collapse("hide")
})


// clothing-hover-state div on mouseleave hide itself
$('#clothing-hover-state').mouseleave(function(){
    $(this).collapse("hide")
})


$('#jewelry').mouseenter(function(){
    $('.nav-link').css({'border-bottom-color': 'transparent'})
    $(this).css({'border-bottom-color': '#6458A2'})
    $('#jewelry-hover-state').collapse("toggle")
     $('#art-hover-state').collapse("hide")
    $('#music-hover-state').collapse("hide")
    $('#clothing-hover-state').collapse("hide")
})

// jewelry-hover-state div on mouseleave hide itself
$('#jewelry-hover-state').mouseleave(function(){
    $(this).collapse("hide")
})


$('#art').mouseenter(function(){
    $('.nav-link').css({'border-bottom-color': 'transparent'})
    $(this).css({'border-bottom-color': '#6458A2'})
    $('#art-hover-state').collapse("toggle")
    $('#clothing-hover-state').collapse("hide")
    $('#jewelry-hover-state').collapse("hide")
    $('#music-hover-state').collapse("hide")
})


// art-hover-state div on mouseleave hide itself
$('#art-hover-state').mouseleave(function(){
    $(this).collapse("hide")
})




$('#music').mouseenter(function(){
    $('.nav-link').css({'border-bottom-color': 'transparent'})
    $(this).css({'border-bottom-color': '#6458A2'})
    $('#music-hover-state').collapse("toggle")
    $('#clothing-hover-state').collapse("hide")
    $('#jewelry-hover-state').collapse("hide")
    $('#art-hover-state').collapse("hide")
    
})


// music-hover-state div on mouseleave hide itself
$('#music-hover-state').mouseleave(function(){
    $(this).collapse("hide")
})





$('.show-modal').click(function(){
    $('#signUpModal').modal('show')
})

$('.loginBtn').click(function(){
    $('#loginUpModal').modal('show')
})

$('#add-to-cart-btn').click(function(){
    $('#add-to-cart').modal('show')
})

$('#single-product-owl.owl-carousel').owlCarousel({
    loop:false,
    margin:15,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:2
            
        }
    }
})

$('.owl-carousel').owlCarousel({
    loop:false,
    margin:15,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
            
        }
    }
})

$( ".owl-prev").html(`<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none">
    <path d="M20.3284 11.0001V13.0001L7.50011 13.0001L10.7426 16.2426L9.32842 17.6568L3.67157 12L9.32842 6.34314L10.7426 7.75735L7.49988 11.0001L20.3284 11.0001Z" fill="currentColor"></path>
</svg>`);

$( ".owl-next").html(`<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none">
    <path d="M15.0378 6.34317L13.6269 7.76069L16.8972 11.0157L3.29211 11.0293L3.29413 13.0293L16.8619 13.0157L13.6467 16.2459L15.0643 17.6568L20.7079 11.9868L15.0378 6.34317Z" fill="currentColor"></path>
</svg>`);

$('#page-filter').on('show.bs.collapse', function(){
     $('#show-filter span').text('Hide filter')
})

$('#page-filter').on('hide.bs.collapse', function(){
     $('#show-filter span').text('Show filter')
})

$('#lightSlider1').lightSlider({
    gallery: true,
    item: 1,
    loop: true,
    slideMargin: 0,
    vertical:false,
    verticalHeight:500,
    vThumbWidth:100,
    thumbItem: 6,
    thumbMargin: 20,
    enableTouch:true
});



$('#lightSlider2').lightSlider({
    gallery: true,
    item: 1,
    loop: true,
    slideMargin: 0,
    vertical:false,
    verticalHeight:500,
    vThumbWidth:100,
    thumbItem: 6,
    thumbMargin: 20,
    enableTouch:true
});

$('#product-details-accordion .item-1').on('show.bs.collapse', function(){
    $('a[href="#product-details-accordion .item-1"]').next().removeClass("fa-angle-down").addClass('fa-angle-up')
})

$('#product-details-accordion .item-1').on('hide.bs.collapse', function(){
    $('a[href="#product-details-accordion .item-1"]').next().removeClass("fa-angle-up").addClass('fa-angle-down')
})

$('#product-details-accordion .item-2').on('show.bs.collapse', function(){
    // alert("Hey Bi")
    $('a[href="#product-details-accordion .item-2"]').next().removeClass("fa-angle-down").addClass('fa-angle-up')
})

$('#product-details-accordion .item-2').on('hide.bs.collapse', function(){
    $('a[href="#product-details-accordion .item-2"]').next().removeClass("fa-angle-up").addClass('fa-angle-down')
})


$('#vendor-home .dropdown').on('show.bs.dropdown',function(){

    
    $(this).find('a.nav-link').css({ 
        "border": "1px solid #6458A2","border-bottom": "0px solid #6458A2", 'margin-top': '2px' })
    $(this).find('button.btn').css({ 
        "border": "1px solid #6458A2","border-bottom": "0px solid #6458A2", 'margin-top': '2px' })
    
    $(this).find('div.dropdown-menu').css({
        
        'margin-top': '-1px'
    })
})

$('#vendor-home .dropdown').on('hide.bs.dropdown',function(){

    
    $(this).find('a.nav-link').css({ 
        "border": "0px solid #6458A2","border-bottom": "0px solid #6458A2",  })
    
    $(this).find('button.btn').css({ 
        "border": "0px solid #6458A2","border-bottom": "0px solid #6458A2",  })
    
    $(this).find('div.dropdown-menu').css({
        
        'margin-top': '-0px'
    })
})
